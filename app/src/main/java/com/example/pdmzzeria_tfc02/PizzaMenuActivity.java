package com.example.pdmzzeria_tfc02;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class PizzaMenuActivity extends AppCompatActivity {
    public static final String PIZZA_MENU = "PizzaMenuActivity.PIZZA_MENU";
    private LinearLayout lnl_Menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_menu);
        this.lnl_Menu = findViewById(R.id.lnl_Menu);
        this.buildMenuList();
    }

    private void buildMenuList(){
        for(Pizza pizza: DAOPizzaSingleton.getINSTANCE().getPizza()) {
            View PizzaItemView = getLayoutInflater().inflate(R.layout.pizza_item_view, this.lnl_Menu, false);

            TextView PizzaNamePreview = PizzaItemView.findViewById(R.id.txt_PizzaNamePreview);
            TextView PizzaDesciptionPreview = PizzaItemView.findViewById(R.id.txt_PizzaDescriptionPreview);
            TextView PizzaBaseValuePreview = PizzaItemView.findViewById(R.id.txt_PizzaBaseValuePreview);

            PizzaNamePreview.setText(pizza.getName());
            PizzaDesciptionPreview.setText(pizza.getDescription());
            PizzaBaseValuePreview.setText(pizza.getBaseValue());
            PizzaItemView.setTag(pizza);
            PizzaItemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Pizza p = (Pizza)PizzaItemView.getTag();
                    onClickDetails(p);
                }
            });
            this.lnl_Menu.addView(PizzaItemView);
        }
    }

    private void onClickDetails(Pizza pizza){
        View DialogBody = getLayoutInflater().inflate(R.layout.pizza_info_dialog,null);
        TextView txtPizzaName = DialogBody.findViewById(R.id.txt_NamePizza);
        TextView txtPizzaDescription = DialogBody.findViewById(R.id.txt_PizzaDescription);

        String bpvalue = pizza.getBaseValue();

        float mpvalue = Float.parseFloat(bpvalue);
        float ppvalue = (float) ((0.25) * mpvalue);
        float gpvalue = (float) ((1.25) * mpvalue);

        TextView txtPValue = DialogBody.findViewById(R.id.txt_P_Value);
        TextView txtMValue = DialogBody.findViewById(R.id.txt_M_Value);
        TextView txtGValue = DialogBody.findViewById(R.id.txt_G_Value);

        txtPizzaName.setText(pizza.getName());
        txtPizzaDescription.setText(pizza.getDescription());
        txtPValue.setText(""+ppvalue);
        txtMValue.setText(pizza.getBaseValue());
        txtGValue.setText(""+gpvalue);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.pizza_info);
        builder.setView(DialogBody);
        builder.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which){
                //do nothing
            }

        }).create().show();
    }
}