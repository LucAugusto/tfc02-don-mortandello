package com.example.pdmzzeria_tfc02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class NewPizzaActivity extends AppCompatActivity {
    public static final String RESULT_KEY = "NewPizzaActivity.RESULT_PIZZA";
    private EditText txtPizzaName;
    private EditText txtPizzaDescription;
    private EditText txtPizzaBaseValue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_pizza);
        this.txtPizzaName = findViewById(R.id.txt_PizzaName);
        this.txtPizzaDescription = findViewById(R.id.txt_PizzaIngredients);
        this.txtPizzaBaseValue = findViewById(R.id.txt_PizzaBaseValue);
    }

    public void onClickSave(View view){
        String pizzaName = this.txtPizzaName.getText().toString();
        String pizzaDescription = this.txtPizzaDescription.getText().toString();
        String pizzaBaseValue = this.txtPizzaBaseValue.getText().toString();

        if(pizzaName.isEmpty() || pizzaDescription.isEmpty() || pizzaBaseValue.isEmpty()){
            return;
        }

        Pizza pizza = new Pizza(pizzaName, pizzaDescription, pizzaBaseValue);

        Intent output = new Intent();

        output.putExtra(RESULT_KEY, pizza);
        setResult(RESULT_OK, output);

        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}