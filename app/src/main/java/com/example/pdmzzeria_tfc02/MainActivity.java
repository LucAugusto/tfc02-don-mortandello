package com.example.pdmzzeria_tfc02;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int NEW_PIZZA_REQUEST_CODE = 666;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState != null && savedInstanceState.containsKey(SAVED_INSTANCE_KEY)){
            this.pizzas = savedInstanceState.getParcelableArrayList(SAVED_INSTANCE_KEY);
        }
        else {
            this.pizzas = new ArrayList<>();
        }
    }


    public void onClickInsertPizza(View view){
        Intent InsertPizzaIntent = new Intent(this,NewPizzaActivity.class);
        startActivityForResult(InsertPizzaIntent,NEW_PIZZA_REQUEST_CODE);
    }

    public void onClickMenu(View view){
        Intent PizzaMenuIntent = new Intent(this,PizzaMenuActivity.class);

        startActivity(PizzaMenuIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == NEW_PIZZA_REQUEST_CODE && resultCode == RESULT_OK && data != null){
            Pizza pizzaObj = data.getParcelableExtra(NewPizzaActivity.RESULT_KEY);
            DAOPizzaSingleton.getINSTANCE().addPizza(pizzaObj);
        }
    }
}