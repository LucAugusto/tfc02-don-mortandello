package com.example.pdmzzeria_tfc02;

import java.util.ArrayList;

public class DAOPizzaSingleton {

    private static DAOPizzaSingleton INSTANCE;
    private ArrayList<Pizza> pizzas;

    private DAOPizzaSingleton(){
        this.pizzas = new ArrayList<>();
    }

    public static DAOPizzaSingleton getINSTANCE(){
        if(INSTANCE == null){
            INSTANCE = new DAOPizzaSingleton();
        }
        return INSTANCE;
    }

    protected ArrayList<Pizza> getPizza(){
        return this.pizzas;
    }

    public void addPizza(Pizza pizza){
        this.pizzas.add(pizza);
    }
}
