package com.example.pdmzzeria_tfc02;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza implements Parcelable {

    private String name;
    private String description;
    private String baseValue;


    public Pizza(String name, String description, String baseValue) {
        this.name = name;
        this.description = description;
        this.baseValue = baseValue;
    }



    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBaseValue() {
        return baseValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(baseValue);
    }

    //-------------------------IMPLEMENTAÇÃO PARCELABLE

    protected Pizza(Parcel in) {
        name = in.readString();
        description = in.readString();
        baseValue = in.readString();
    }

    public static final Creator<Pizza> CREATOR = new Creator<Pizza>() {
        @Override
        public Pizza createFromParcel(Parcel in) {
            return new Pizza(in);
        }

        @Override
        public Pizza[] newArray(int size) {
            return new Pizza[size];
        }
    };
}
